PROJECT INFO
=====================
TASK MANAGER

DEVELOPER INFO
=====================
**NAME:** Vsevolod Zorin

**E-MAIL:** <seva89423@gmail.com>

STACK OF TECHNOLOGIES
=====================
- TypeScript
- Nest.js
- TypeORM
- PostgreSQL
- Git Bash
- Insomnia 

SOFTWARE
=====================
- Microsoft Visual Studio Code
- MS WINDOWS 10

ADDITIONAL DEPENDENCIES
=====================
```
# TypeORM Connection
npm i typeorm pg @nestjs/typeorm

# NestJS Connection
npm i @nestjs/cli -g

```

PROGRAM BUILD
=====================
```
npm install
```

PROGRAM RUN
=====================
```
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
