import { IsNumber, IsString } from "class-validator";

export class TaskDTO {

    @IsNumber() userId:number;
  
    @IsNumber() readonly taskId:number;

    @IsString() taskName: string;
  
    @IsString() taskDescription: string;
  
    constructor(taskName: string, taskDescription: string, taskId?: number) {
      this.taskName = taskName;
      this.taskDescription = taskDescription;
      this.taskId = taskId;
    }
  }