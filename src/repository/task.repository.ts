import { Injectable } from "@nestjs/common"
import { ITaskRepository } from "src/interface/repository/ITaskRepository";
import { In } from "typeorm";
import { Task } from "../entity/task.model";

@Injectable()
export class TaskRepository implements ITaskRepository
{
    async findAllTasks(token: any)
    {
        return await Task.find({ userId: In([token])});
    }

    async createTask(task: any)
    {
        await Task.getRepository().save(task);
    }

    async updateTask(task:any)
    {
        await Task.getRepository().save(task);
    }

    async deleteTask(task:any)
    {
        await Task.getRepository().remove(task);
    }
}