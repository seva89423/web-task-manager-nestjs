import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { IUserRepository } from 'src/interface/repository/IUserRepository';
import { Task } from 'src/entity/task.model';
import { User } from 'src/entity/user.model';
import { In } from 'typeorm';

export let access_token: string;
export let payload: any;

@Injectable()
export class UserRepository implements IUserRepository
{
    constructor(private jwtService: JwtService)
    {
  
    }

    async findOneByName(userName: string)
    {
        let expectedUser = User.findOne({ username: In([userName])});
        if (expectedUser == undefined) return;
        return expectedUser;
    }

    async validateUser(username: string, pass: string): Promise<any> 
    {
        const user = await this.findOneByName(username);
        if (user && user.password === pass) {
          const { password, ...result } = user;
          return result;
        }
        return null;
      }
    
      async logout()
      {
        payload = undefined;
        return payload;
      }
    
      async login(user: any) 
      {
        payload = { username: user.username, sub: user.userId };
        return {
          access_token: this.jwtService.sign(payload),
        };
      }
    
      async registry(user: any)
      {
        await User.getRepository().save(user);
        return this.login(user);
      }
    
      async changePassword(user: any)
      {
        await User.getRepository().save(user);
      }
    
      async changeUsername(user: any)
      {

        await User.getRepository().save(user);
      }
     
      async delete(user: any, tasks: any)
      {
        await User.getRepository().delete(user);
        await Task.getRepository().delete(tasks);
        payload = undefined;
        throw new UnauthorizedException();
      }

      async getToken(): Promise<string>
      {
        if (payload == undefined) throw new UnauthorizedException(); 
        return payload.sub;
      }
}