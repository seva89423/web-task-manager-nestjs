import { Module } from '@nestjs/common';
import { AppController } from '../controller/app.controller';
import { AppService } from '../service/app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TaskController } from '../controller/task.controller';
import { Task } from '../entity/task.model';
import { TaskService } from '../service/task.service';
import { TaskRepository } from '../repository/task.repository';
import { User } from '../entity/user.model';
import { UserService } from '../service/user.service';
import { LocalStrategy } from '../auth/local.strategy';
import { UserController } from '../controller/user.controller';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants, timeConst } from '../auth/constants';
import { JwtStrategy } from '../auth/jwt.strategy';
import { UserRepository } from '../repository/user.repository';

@Module({
  imports: [ TypeOrmModule.forRoot({
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'test',
    password: 'test',
    database: 'TaskBase',
    logging: true,
    synchronize: true,
    entities: [Task, User],
  }), JwtModule.register({
    secret: jwtConstants.secret,
    signOptions: { expiresIn: timeConst.time },
  }),],
  controllers: [AppController, TaskController, UserController],
  providers: [AppService, TaskService, TaskRepository, UserService, LocalStrategy, JwtStrategy, UserRepository],
})
export class AppModule {}
