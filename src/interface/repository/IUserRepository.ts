export interface  IUserRepository
{
    findOneByName(userName: string):any;
    validateUser(username: string, pass: string): Promise<any>;
    logout():any;
    login(user: any):any; 
    registry(user: any):any;  
    changePassword(user: any):any;
    changeUsername(user: any):any;   
    delete(user: any, tasks: any):any;
    getToken(): Promise<string>;
}