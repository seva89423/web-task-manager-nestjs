export interface ITaskRepository
{
    findAllTasks(token: any): any;
    createTask(task: any): any;
    updateTask(task:any): any;
    deleteTask(task:any): any;
}