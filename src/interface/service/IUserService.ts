import { UserDTO } from "src/dto/user.dto";

export interface IUserService 
{
    allUsers(): any;
    findOneByName(userName: string): any;
    validateUser(username: string, pass: string): Promise<any>;    
    logout(): any;    
    login(user: any): any;
    registry(user: UserDTO): any;    
    changePassword(token: string, newPassword: string): any;    
    changeUsername(token: string, newUsername: string): any;
    delete(token: string): any;
    getToken(): Promise<string>;
}