import { TaskDTO } from "src/dto/task.dto";

export interface ITaskService 
{
    findAllTasks(token: any): any;
    createTask(task: TaskDTO, user: any): any;
    updateTask(task: TaskDTO, user: any): any;
    deleteTask(taskId:number, user: any): any;
}