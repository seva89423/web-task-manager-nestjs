import { TaskDTO } from "src/dto/task.dto";

export interface ITaskController 
{
    findAll(): any;
    create(task: TaskDTO): any;
    update(task: TaskDTO): any;
    delete(body: any): any;
}