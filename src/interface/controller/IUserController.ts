import { UserDTO } from "src/dto/user.dto";

export interface IUserController
{
    login(req: any): any;
    registry (user: UserDTO): any;
    changePassword(body: any): any;
    changeUsername(body: any): any;
    deleteUser(): any;
    logout(): any;
    getProfile(req: any): any;
}