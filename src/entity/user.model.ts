import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'users' })
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  readonly userId: number;

  @Column()
  username: string;

  @Column()
  password: string;

  constructor(username: string, password: string, userId?: number) {
    super();
    this.userId = userId;
    this.username = username;
    this.password = password;
  }
}