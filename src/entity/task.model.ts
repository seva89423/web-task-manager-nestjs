import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'tasks' })
export class Task extends BaseEntity {
  @PrimaryGeneratedColumn()
  readonly id: number;

  @Column()
  taskName: string;

  @Column()
  readonly userId: number;

  @Column()
  taskDescription: string;

  constructor(taskName: string, taskDescription: string, userId?: number, id?: number) {
    super();
    this.id = id;
    this.userId = userId;
    this.taskName = taskName;
    this.taskDescription = taskDescription;
  }
}