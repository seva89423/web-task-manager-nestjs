import { Injectable } from '@nestjs/common';
import { Task } from 'src/entity/task.model';
import { UserDTO } from '../dto/user.dto';
import { User } from '../entity/user.model';
import { UserRepository } from '../repository/user.repository';
import { IUserService } from 'src/interface/service/IUserService';

@Injectable()
export class UserService implements IUserService
{
    constructor(private usersRepository: UserRepository)
    {

    }

    async allUsers()
    {
        return await User.find();
    }

    async findOneByName(userName: string)
    {
        return this.usersRepository.findOneByName(userName);
    }

    async validateUser(username: string, pass: string): Promise<any> 
    {
        return this.usersRepository.validateUser(username, pass);
    }
    
    async logout()
    {
        return this.usersRepository.logout();
    }
    
    async login(user: any) 
    {
        return this.usersRepository.login(user);
    }

    async registry(user: UserDTO)
    {
        let newUser = new User(user.username, user.password, User.length + 1);
        return this.usersRepository.registry(newUser);
    }
    
    async changePassword(token: string, newPassword: string)
    {
        let user = await User.findOne(token);
        if (user == undefined) return;
        user.password = newPassword;
        return this.usersRepository.changePassword(user);
    }
    
    async changeUsername(token: string, newUsername: string)
    {
        let user = await User.findOne(token);
        if (user == undefined) return;
        user.username = newUsername;
        return this.usersRepository.changeUsername(user);
    }
    
    async delete(token: string)
    {
        let user = await User.findOne(token);
        if (user == undefined) return;
        let tasksToDelete = await Task.getRepository().find({where: { userId: user.userId }});
        return this.usersRepository.delete(user, tasksToDelete);
    } 

    async getToken(): Promise<string>
    {
        return this.usersRepository.getToken();
    }
}