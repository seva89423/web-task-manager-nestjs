import { Injectable } from "@nestjs/common";
import { ITaskService } from "src/interface/service/ITaskService";
import { TaskDTO } from "../dto/task.dto";
import { Task } from "../entity/task.model";
import { TaskRepository } from "../repository/task.repository";

@Injectable()
export class TaskService implements ITaskService
{
    constructor(private readonly repository: TaskRepository)
    {

    }

    async findAllTasks(token: any)
    {
        return this.repository.findAllTasks(token);
    }

    async createTask(task: TaskDTO, user: any)
    {
        let newtask = new Task(task.taskName, task.taskDescription, user.userId);
        return this.repository.createTask(newtask);
    }

    async updateTask(task: TaskDTO, user: any)
    {
        let taskUpdate = await Task.getRepository().findOne({where: { id: task.taskId, userId: user.userId }});
        if (taskUpdate == undefined) return "Task does not exist";
        taskUpdate.taskName = task.taskName;
        taskUpdate.taskDescription = task.taskDescription;
        return this.repository.updateTask(taskUpdate);
    }

    async deleteTask(taskId:number, user: any)
    {
        let task = await Task.getRepository().findOne({where: { id: taskId, userId: user.userId }});
        if (task == undefined) return "Task does not exist";
        return this.repository.deleteTask(task);
    }
}