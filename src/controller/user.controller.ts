import { Post, Controller, Request, UseGuards, Body, Get, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { IUserController } from 'src/interface/controller/IUserController';
import { UserDTO } from '../dto/user.dto';
import { UserService } from '../service/user.service';

export let token: any;

@Controller('user')
export class UserController implements IUserController{
    constructor(private userService: UserService) 
    {

    }
    
    @UseGuards(AuthGuard('local'))
    @Post('auth')
    async login(@Request() req) 
    {
      return this.userService.login(req.user);
    }

    @Post('registry')
    async registry(@Body() user: UserDTO) 
    {
      return this.userService.registry(user);
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('change-password')
    async changePassword(@Body() body: any) 
    {
      token = await this.userService.getToken();
      if (token == null) throw new UnauthorizedException();
      return this.userService.changePassword((token).toString(), body.newPassword);
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('change-username')
    async changeUsername(@Body() body: any) 
    {
      token = await this.userService.getToken();
      if (token == null) throw new UnauthorizedException();
      return this.userService.changeUsername((token).toString(), body.newUsername);
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('remove')
    async deleteUser() 
    {
      token = await this.userService.getToken();
      if (token == null) throw new UnauthorizedException();
      return this.userService.delete((token).toString());
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('logout')
    async logout() 
    {
      return this.userService.logout();
    }

    @UseGuards(AuthGuard('jwt'))
    @Get('info')
    getProfile(@Request() req) 
    {
      return req.user;
    }
}