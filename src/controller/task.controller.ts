import { Get, Post, Controller, Body, Put, Delete, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { User } from 'src/entity/user.model';
import { ITaskController } from 'src/interface/controller/ITaskController';
import { UserService } from 'src/service/user.service';
import { TaskDTO } from '../dto/task.dto';
import { TaskService } from '../service/task.service';
export let token: any;

@Controller('tasks')
export class TaskController implements ITaskController{
  constructor(private readonly service: TaskService, private readonly userService: UserService) {

  }

  @UseGuards(AuthGuard('jwt'))
  @Get()
  async findAll() {
    token = await this.userService.getToken();
    return this.service.findAllTasks(token);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('create')
  async create(@Body() task: TaskDTO) {
    token = await this.userService.getToken();
    let user = await User.findOne(token);
    return this.service.createTask(task, user);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put('update')
  async update(@Body()  task: TaskDTO) {
    token = await this.userService.getToken();
    task.userId = token;
    let user = await User.findOne(token);
    return this.service.updateTask(task, user);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete('delete')
  async delete(@Body() body: any) {
    token = await this.userService.getToken();
    let user = await User.findOne(token);
    return this.service.deleteTask(body.taskId, user);
  }
}